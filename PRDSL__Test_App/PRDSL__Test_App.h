/*
 *  Copyright Oct 24, 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 */

#ifndef _PRDSL_TEST_APP_H_
#define _PRDSL_TEST_APP_H_

#include <stdio.h>

#include "../PR_defs__turn_on_and_off.h"
#include <PR__include/prmalloc.h>
#include <PR__include/langlets/prdsl_wrapper_library.h>

//===============================  Defines  ==============================

//==============================  Structures  ==============================

typedef struct
 { 
   int32  *data;
 }
SeedParams;

typedef struct
 { 
   int32  start;
   int32  end;
   int32 *data;
 }
TaskParams;

//============================= Processor Functions =========================
void test_app_seed_Fn(      void *data, SlaveVP *animatingSlv ); //seed VP function
void task_birthFn( void  *_params, SlaveVP *animVP );


//================================ Entry Point ==============================
void
PRDSL__Test_App( );

//================================ Global Vars ==============================

#endif /*_SSR_MATRIX_MULT_H_*/
