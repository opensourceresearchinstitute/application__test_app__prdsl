/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */


#include <math.h>
#include <string.h>
#include "PRDSL__Test_App.h"
 
int 
square( int x ) 
 { return x*x; 
 }
   
void task_birthFn( void  *_params, SlaveVP *animVP )
 { int32 i;
   TaskParams *params = (TaskParams *)_params;
   int32 *data = params->data;
         DEBUG__printf(TRUE, "Task %d", params->start);
   for( i=params->start; i < params->end; ++i ) 
    { data[i] = square(i);
    }
   PRDSL__end_task( animVP );
 }

