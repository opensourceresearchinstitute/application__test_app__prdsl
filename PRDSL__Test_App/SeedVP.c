/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */

#include <math.h>
#include <string.h>
#include "PRDSL__Test_App.h"


/*
   ------- Original Source, using custom DSL syntax -------
   int size = 1000;
   int data[];
   data = makeArray( size );

   IterateIndependently i in 1:size
    { data[i] = square( i );
    }
 */

void 
test_app_seed_Fn( void *_params, SlaveVP *seedVP )
 { PRDSLTaskStub *newTask;
   int size = 1000;
   int *data = (int *) PR__malloc( size * sizeof(int) ) ;
   SeedParams *seedParams = (SeedParams *)_params; //used to comm with main()

   PRDSL__start( seedVP ); //starts the PR_DSL langlet -- does internal stuff inside proto-runtime..
   //can start additional languages here, and then freely mix "constructs" from them  

   int i ;
   TaskParams *params;
   int chunkSize = 100; //for portability, lang would choose this dynamically at run time
   for (i=0; i < size; i += chunkSize) 
    { params = PR__malloc(sizeof(TaskParams)); //these define work the task performs
      params->start = i;
      params->end = i + chunkSize -1;
      params->data = data;
      newTask = 
          PRDSL__create_task_ready_to_run( &task_birthFn, params, seedVP );
    }

   //PRDSL__wait_for_all_children_to_end( animSlv ); //bug prone -- children must do same
   PRDSL__wait_for_all_PRDSL_created_work_to_end( seedVP );
   
   seedParams->data = data; //sends results back to main()
   
   PRDSL__shutdown( seedVP ); //Shuts down PRDSL within the process

      //This ends the last live entity capable of work, in a process
      // that has no external input ports.. hence, no activity can take place
      // past that point..  PR detects that, and then automatically ends the
      // process.
   PR__end_seedVP( seedVP );
 }

