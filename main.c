/*
 *  Copyright 2012 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * author seanhalle@yahoo.com
 */

#include <malloc.h>
#include <stdlib.h>

#include "PRDSL__Test_App/PRDSL__Test_App.h"
#include <PR__include/PR__WL.h>  //declares PR__create_process


/*This demonstrates the use of the proto-runtime system.  It allows multiple
 * languages to be mixed within a single sub-program.  It also allows multiple
 * sub-programs to be started, where each uses its own set of languages. The
 * running sub-programs can then communicate with each other.
 * 
 */
int main( int argc, char **argv )
 { PRProcess *testProcess1, *testProcess2;
 
   DEBUG__printf(TRUE, "arguments: %s | %s", argv[0], argv[1] );
   
      //A proto-runtime based language sits on top of the proto-runtime. So, 
      // first start the proto-runtime system, then create processes (which
      // start languages inside themselves)
   PR__start();   
  
      //Now that PR is started, create processes.  
      //Each process creates a seedVP and starts it running -- that then starts
      // the languages used inside the process..
      //To get results from a process, it gets complicated..  simple soln is 
      // just use PR's malloc and free, in the main thread, between PR__start
      // and PR__shutdown
      //The call returns a process struct (which has access to the seedVP)
   int32 *result = PR__malloc( 2 * sizeof(int32) );
   testProcess1 = PR__create_process( &test_app_seed_Fn, result );
         
   PR__wait_for_process_to_end( testProcess1 );
   printf("\n\nresults: %d, %d\n\n", result[0], result[1] );
   
   PR__free(result);
   
   PR__wait_for_all_activity_to_end();  //equivalent of detecting shutdown
   PR__shutdown();
   
   exit(0); //cleans up
 }
